CONVERSION = {
    'I': 1,
    'V': 5,
    'X': 10,
    'L': 50,
    'C': 100,
    'D': 500,
    'M': 1000,
}


def roman_to_int(text: str):
    text = text[::-1]
    result = 0
    previous = -1
    for letter in text:
        number = CONVERSION[letter]

        if number < abs(previous):
            number = -number

        result += number
        previous = number

    return result


print(roman_to_int('MDCCLXXXIII'))
