def default(a, b=None):
    b = (a if b is None else b)
    print(f'a={a}, b={b}')


default(1)
default(1, 'c')
