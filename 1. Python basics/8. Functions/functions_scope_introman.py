CONVERSION = {
    'I': 1,
    'II': 2,
    'III': 3,
    'IV': 4,
    'V': 5,
    'VI': 6,
    'VII': 7,
    'VIII': 8,
    'IX': 9,
    'X': 10,
    'XX': 20,
    'XXX': 30,
    'XL': 40,
    'L': 50,
    'LX': 60,
    'LXX': 70,
    'LXXX': 80,
    'XC': 90,
    'C': 100,
    'CC': 200,
    'CCC': 300,
    'CD': 400,
    'D': 500,
    'DC': 600,
    'DCC': 700,
    'DCCC': 800,
    'DM': 900,
    'M': 1000,
}


def get_key(search_value):
    return [key for key, value in CONVERSION.items() if value == search_value]


def int_to_roman(number):
    number = str(number)[::-1]

    result = []
    counter = 1
    for element in number:
        element = int(element) * counter
        result.append(get_key(element)[0])
        counter *= 10

    result = result[::-1]
    return ''.join(result)


print(int_to_roman(1584))
