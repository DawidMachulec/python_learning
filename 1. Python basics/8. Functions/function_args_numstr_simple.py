DATA = {
    0: 'zero',
    1: 'one',
    2: 'two',
    3: 'tree',
    4: 'fower',
    5: 'fife',
    6: 'six',
    7: 'seven',
    8: 'ait',
    9: 'niner',
}

data = {str(k): v for k, v in DATA.items()}


def pilot_say(text):
    result =[]
    for number in str(text):
        if number == '\n':
            continue

        if number == '-':
            result.append('minus')
        elif number == '.':
            result.append('and')
        else:
            result.append(data[number])

    return ' '.join(result)


print(pilot_say(-35.67))
