def km_to_m(distance):
    """
    >>> km_to_m(-1)
    Traceback (most recent call last):
        ...
    TypeError: Distance cannot be negative
    >>> km_to_m(0)
    0.0
    >>> km_to_m(1)
    1000.0
    >>> km_to_m(56.14)
    56140.0
    >>> km_to_m('a')
    Traceback (most recent call last):
        ...
    TypeError: Distance must be float or int
    >>> km_to_m([1, 1])
    Traceback (most recent call last):
        ...
    TypeError: Distance must be float or int
    >>> km_to_m(True)
    Traceback (most recent call last):
        ...
    TypeError: Distance must be float or int
    """
    km = 1000

    if type(distance) not in (float, int):
        raise TypeError('Distance must be float or int')

    if distance < 0:
        raise TypeError('Distance cannot be negative')

    return float(distance*km)
