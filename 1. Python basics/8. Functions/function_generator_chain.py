def odd(number):
    """
    >>> odd(1)
    True
    >>> odd(2)
    False
    >>> odd(3)
    True
    >>> odd(4)
    False
    """
    return number % 2 != 0


def cube(number):
    """
    >>> cube(2)
    8
    >>> cube(3)
    27
    """
    return number ** 3


numbers = list(range(3, 34, 3))
numbers = filter(odd, numbers)
numbers = map(cube, numbers)
numbers = list(numbers)
result = sum(numbers)/len(numbers)
print(result)
