def celsius_to_kelvin(degrees):
    """
    >>> celsius_to_kelvin(0)
    273.15
    >>> celsius_to_kelvin(1)
    274.15
    >>> celsius_to_kelvin(-1)
    272.15
    >>> celsius_to_kelvin('a')
    Traceback (most recent call last):
        ...
    TypeError: Invalid argument
    >>> celsius_to_kelvin([0, 1])
    [273.15, 274.15]
    >>> celsius_to_kelvin((0, 1))
    (273.15, 274.15)
    >>> celsius_to_kelvin({0, 1})
    {273.15, 274.15}
    """
    datatype = type(degrees)

    if datatype in {list, tuple, set, frozenset}:
        return datatype(celsius_to_kelvin(element) for element in degrees)

    if type(degrees) not in (int, float):
        raise TypeError('Invalid argument')

    return degrees + 273.15
