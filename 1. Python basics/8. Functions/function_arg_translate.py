PL = {'ą': 'a', 'ć': 'c', 'ę': 'e',
      'ł': 'l', 'ń': 'n', 'ó': 'o',
      'ś': 's', 'ż': 'z', 'ź': 'z'}


def translate(text):
    result = (PL.get(x, x) for x in text)
    return ''.join(result)


print(translate('zażółć'))
