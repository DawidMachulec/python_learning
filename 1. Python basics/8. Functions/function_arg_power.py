def power(a, b=None):
    return a**a if b is None else a**b


print(power(4, 3))
print(power(3))
