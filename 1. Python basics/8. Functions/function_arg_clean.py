def clean(text: str):
    text = text.strip().upper().split()
    result = []
    for element in text:
        if element.startswith('UL'):
            continue

        if '3' in element:
            element = 'III'

        result.append(element)

    return ' '.join(result)



print(clean('ul.Mieszka II'))
print(clean('UL. Zygmunta III WaZY'))
print(clean('  bolesława chrobrego '))
print(clean('ul Jana III SobIESkiego'))
print(clean('\tul. Jana trzeciego Sobieskiego'))
print(clean('ulicaJana III Sobieskiego'))
print(clean('UL. JA    NA 3 SOBIES  KIEGO'))
print(clean('ULICA JANA III SOBIESKIEGO  '))
print(clean('ULICA. JANA III SOBIeskieGO'))
print(clean(' Jana 3 Sobieskiego  '))
print(clean('Jana III Sobi  eskiego '))
