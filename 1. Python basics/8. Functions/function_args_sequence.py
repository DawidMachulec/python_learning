def add_even(sequence):
    return sum(x for x in sequence if x % 2 == 0)


print(add_even([1, 2, 3, 4, 5]))
