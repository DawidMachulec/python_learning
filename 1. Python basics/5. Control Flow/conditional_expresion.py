blood_pressure = input("Type your blood pressure in format XXX/YY: ")

xxx, yy = blood_pressure.split('/')
xxx, yy = int(xxx), int(yy)

if xxx > 180 or yy > 120:
    print("Hypertensive Crisis")
elif xxx >= 140 or yy >= 90:
    print("Hypertension stage 2")
elif xxx >= 130 or yy >= 80:
    print("Hypertension stage 1")
elif xxx >= 120:
    print("Elevated")
else:
    print("Normal")
