FILE = r'_temporary.txt'
DATA = 'hello world'

with open(FILE, mode='w') as file:
    file.write(DATA)

with open(FILE, mode='r') as file:
    result = file.read()

print(result)
