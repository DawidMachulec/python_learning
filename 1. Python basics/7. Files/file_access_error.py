path = input('Type the file directory: ')

try:
    with open(path) as file:
        print(file.read())

except FileNotFoundError:
    print('File does not exist')

except PermissionError:
    print('Permission denied')