from pathlib import Path

path = input('Type the path: ')

path = path.strip()
path = Path(Path.cwd(), path)

print(f'information about path: \n '
      f'If path exist: {path.exists()} \n'
      f'If it is catalogue: {path.is_dir()} \n'
      f'If it is file: {path.is_file()}')
