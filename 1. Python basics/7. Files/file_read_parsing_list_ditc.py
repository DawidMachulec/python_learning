from pprint import pprint

FILE = r'hosts-advanced.txt'
DATA = """
##
# ``/etc/hosts`` structure:
#   - IPv4 or IPv6
#   - Hostnames
 ##

127.0.0.1       localhost
127.0.0.1       astromatt
10.13.37.1      nasa.gov esa.int roscosmos.ru
255.255.255.255 broadcasthost
::1             localhost
"""

result = []

with open(FILE, mode='wt') as file:
    file.write(DATA)

try:
    with open(FILE) as file:
        content = file.readlines()
except FileNotFoundError:
    print('File does not exist')
except PermissionError:
    print('You have no permission')

for line in content:
    row = {}
    if not line.startswith((' ', '#', '\n')):
        ip, *hosts = line.strip().split()

        row['IP'] = ip
        row_type = ('IPv4' if '.' in ip else 'IPv6')
        row[row_type] = hosts
        result.append(row)

pprint(result)
