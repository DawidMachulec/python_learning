FILE = r'_temporary.csv'
DATA = """sepal_length,sepal_width,petal_length,petal_width,species
5.4,3.9,1.3,0.4,setosa
5.9,3.0,5.1,1.8,virginica
6.0,3.4,4.5,1.6,versicolor
7.3,2.9,6.3,1.8,virginica
5.6,2.5,3.9,1.1,versicolor
5.4,3.9,1.3,0.4,setosa
5.5,2.6,4.4,1.2,versicolor
5.7,2.9,4.2,1.3,versicolor
4.9,3.1,1.5,0.1,setosa
6.7,2.5,5.8,1.8,virginica
6.5,3.0,5.2,2.0,virginica
5.1,3.3,1.7,0.5,setosa
"""

header = []
features = []
labels = []

with open(FILE, mode='wt') as file:
    file.write(DATA)

with open(FILE, mode='rt') as file:
    headers, *values = file.readlines()
    [header.append(x) for x in headers.strip().split(',')]
    for row in values:
        *feature, label = row.strip().split(',')
        features.append(tuple(feature))
        labels.append(label)

print(header)
print(features)
print(labels)

