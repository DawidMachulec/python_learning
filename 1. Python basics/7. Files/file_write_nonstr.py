FILE = r'_temporary.txt'
DATA = (5.1, 3.5, 1.4, 0.2, 'setosa')

data = ' '.join(str(x) for x in DATA)

with open(FILE, mode='w') as file:
    file.write(data)
