from pprint import pprint

KEYS = ['Sepal length', 'Sepal width', 'Petal length', 'Petal width', 'Species']
VALUES = [5.8, 2.7, 5.1, 1.9, 'virginica']

result = dict(zip(KEYS, VALUES))

pprint(result)
