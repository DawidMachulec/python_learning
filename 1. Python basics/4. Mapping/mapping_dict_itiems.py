from pprint import pprint

DATA = {
    'Sepal length': 5.8,
    'Sepal width': 2.7,
    'Petal length': 5.1,
    'Petal width': 1.9,
}

keys = list(DATA.keys())
values = list(DATA.values())
items = list(DATA.items())

print(f'keys: {keys}\n'
      f'values: {values}\n'
      f'items: {items}')