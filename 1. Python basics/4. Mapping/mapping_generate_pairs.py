from pprint import pprint

DATA = [
    ('Sepal length', 5.8),
    ('Sepal width', 2.7),
    ('Petal length', 5.1),
    ('Petal width', 1.9),
    ('Species', 'virginica')
]

result = dict(DATA)

pprint(result)
