class Astronaut:
    def __init__(self, name, surname, nation, birth):
        self.birth = birth
        self.nation = nation
        self.surname = surname
        self.name = name


class Company:
    def __init__(self, name, country, established):
        self.established = established
        self.country = country
        self.name = name


astronaut_1 = Astronaut('Mark', 'Watney', 'USA', '1969-07-21')
astronaut_2 = Astronaut('Jan', 'Twardowski', 'Poland', '1961-04-12')
company_1 = Company('National Aeronautics and Space Administration', 'USA', '1958-07-29')
company_2 = Company('Polish Space Agency', 'Poland', '2014-09-26')

print(astronaut_1.__dict__)
print(astronaut_2.__dict__)
print(company_1.__dict__)
print(company_2.__dict__)
