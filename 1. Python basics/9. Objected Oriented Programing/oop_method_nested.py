class Iris:
    def __init__(self, sepal_length, sepal_width, petal_length, petal_width, species):

        self.sepal_length = sepal_length
        self.sepal_width = sepal_width
        self.petal_length = petal_length
        self.petal_width = petal_width
        self.species = species

    def get_species(self):
        return self.species

    def float_value(self):
        return [x for x in self.__dict__.values() if isinstance(x, (float, int))]

    def amount_of_float(self):
        return len(self.float_value())

    def sum_of_float(self):
        return sum(self.float_value())

    def mean_of_float(self):
        return self.sum_of_float() / self.amount_of_float()

    def show(self):
        return f'total={self.sum_of_float():.2f} mean={self.mean_of_float():.2f} {self.species}'


setosa = Iris(5.1, 3.5, 1.4, 0.2, 'setosa')
virginica = Iris(5.8, 2.7, 5.1, 1.9, 'virginica')

print(setosa.show())
print(virginica.show())
