"""
>>> from inspect import isclass
>>> assert isclass(Engineer)
>>> assert isclass(Scientist)
>>> assert isclass(Pilot)
>>> assert issubclass(Astronaut, Engineer)
>>> assert issubclass(Astronaut, Scientist)
>>> assert issubclass(Astronaut, Pilot)
"""


class Engineer:
    pass


class Scientist:
    pass


class Pilot:
    pass


class Astronaut(Engineer, Scientist, Pilot):
    pass
