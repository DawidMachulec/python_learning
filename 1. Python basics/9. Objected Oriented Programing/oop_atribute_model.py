class Astronaut:
    pass


class Company:
    pass


astronaut_1 = Astronaut()
astronaut_1.name = 'Mark'
astronaut_1.surname = 'Wetney'
astronaut_1.nation = 'USA'
astronaut_1.birth = '1969-07-21'

astronaut_2 = Astronaut()
astronaut_2.name = 'Jan'
astronaut_2.surname = 'Twardowski'
astronaut_2.nation = 'Polend'
astronaut_2.birth = '1961-04-12'

company_1 = Company()
company_1.name = 'National Aeronautics and Space Administration'
company_1.country = 'USA'
company_1.established = '1958-07-29'

company_2 = Company()
company_2.name = 'Polish Space Agency'
company_2.country = 'Poland'
company_2.established = '2014-09-26'

print(astronaut_1.__dict__)
print(astronaut_2.__dict__)
print(company_1.__dict__)
print(company_2.__dict__)
