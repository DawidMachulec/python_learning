DATA = [
    (4.7, 3.2, 1.3, 0.2, 'setosa'),
    (7.0, 3.2, 4.7, 1.4, 'versicolor'),
    (7.6, 3.0, 6.6, 2.1, 'virginica'),
]


class Iris:
    def __init__(self, features, label):
        self.label = label
        self.features = features

    def sum_all(self):
        return sum(self.features)

    def get_label(self):
        return self.label


for *values, species in DATA:
    flower = Iris(values, species)
    print(f'{flower.get_label()} {flower.sum_all()}')
