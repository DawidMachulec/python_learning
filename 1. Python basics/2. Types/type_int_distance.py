M = 1
KM = 1000*M

# in km
AMSTRONG_LINE = 18
ASTROSPHERE = 20
USAF_SPACE_LINE = 80

# in m
EARTH = 100000
MARS = 80000
VENUS = 250000


print(f'Armstrong Line: {AMSTRONG_LINE * KM} m')
print(f'Stratosphere: {ASTROSPHERE * KM} m')
print(f'USAF Space: {USAF_SPACE_LINE * KM} m')
print(f'Kármán Line Earth: {EARTH // KM} km')
print(f'Kármán Line Mars: {MARS // KM} km')
print(f'Kármán Line Venus: {VENUS // KM} km')
