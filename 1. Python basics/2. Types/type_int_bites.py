B = 1
KB = 1024 * B
MB = 1024 * KB

file_size = 1 * MB

print(f'size: {file_size} b')
print(f'size: {file_size // KB} kb')
