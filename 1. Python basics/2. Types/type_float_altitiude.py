FT = 0.3048  # m

altitude = 10000 * FT

print(f'Altitude: {altitude:.1f}')
