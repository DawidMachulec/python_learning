M = 1
KM = 1000 * M
MILE = 1609.344 * M
NAUTICAL_MILE = 1852 * M

m = 1337

print(f'Meters: {m}')
print(f'Kilometers: {m / KM}')
print(f'Miles: {m / MILE}')
print(f'Nautical Miles: {m / NAUTICAL_MILE}')
print(f'm: {m}, km: {int(m / KM)}, mi: {(m / MILE):.1f}, nm: {(m / NAUTICAL_MILE):.2f}')
