b = 1
Kb = 1024 * b
Mb = 1024 * Kb
B = 8 * b
KB = 1024 * B
MB = 1024 * KB

file_size = 100 * MB
speed = 100 * Mb  # Mb/s

print(f'File size: {file_size // MB} MB')
print(f'Download speed: {speed // MB} MB/s')
print(f'Download time: {file_size // speed} s')
