M = 1
KM = 1000 * M
MILE = 1609.344 * M

velocity = 75 * MILE

print(f'Speed limit: {(velocity / KM):.1f} km/h')
