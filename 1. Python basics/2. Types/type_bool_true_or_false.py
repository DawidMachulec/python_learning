a = bool(False)
b = bool(True)

c = bool(0)
d = bool(0.0)
e = bool(-0)
f = bool(-0.0)

g = bool('a')
h = bool('.')
i = bool('0')
j = bool('0.0')
k = bool('')
l = bool(' ')

m = bool(int('0'))
n = bool(float(str(-0)))

o = bool(-0.0+0.0j)
p = bool('-0.0+0.0j')
q = bool(complex('-0.0+0.0j'))