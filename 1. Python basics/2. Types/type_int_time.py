S = 1
MIN = 60  # sec
H = 60  # min

DAY = 24  # h
WORK_DAY = 8  # h
WORK_WEEK = 5  # days
WORK_MONTH = 22  # days

print(f'Day: {DAY * H * MIN} sec')
print(f'Day: {DAY * H} min')
print(f'Work day: {WORK_DAY * H * MIN} sec')
print(f'Work week: {WORK_WEEK * WORK_DAY * H} min')
print(f'Work moth: {WORK_MONTH * WORK_DAY} h')