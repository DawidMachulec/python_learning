b = 1
Kb = 1024 * b
Mb = 1024 * Kb
B = 8 * b
KB = 1024 * B
MB = 1024 * KB

file_size = 1 * MB

print(f'Size: {file_size // MB} MB')
print(f'Size: {file_size // Mb} Mb')