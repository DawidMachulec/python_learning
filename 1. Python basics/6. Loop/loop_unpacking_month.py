from pprint import pprint

MONTHS = ['January', 'February', 'March', 'April',
          'May', 'June', 'July', 'August', 'September',
          'October', 'November', 'December']

enum_months = dict()

for i, month in enumerate(MONTHS):
    enum_months[i+1] = month

pprint(enum_months)