from pprint import pprint

MONTHS = ['January', 'February', 'March', 'April',
          'May', 'June', 'July', 'August', 'September',
          'October', 'November', 'December']

result = dict()

for i, element in enumerate(MONTHS, start=1):
    if i < 10:
        number = '0' + str(i)
        result[number] = element
    else:
        number = str(i)
        result[number] = element

pprint(result)
