from pprint import pprint

DATA = ['red', 'green', 'blue', 'red', 'green', 'red', 'blue']

color = dict()

for obj in DATA:
    if obj not in color.keys():
        color[obj] = 1
    else:
        color[obj] += 1

pprint(color)
