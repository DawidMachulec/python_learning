GRADE_SCALE = (2.0, 3.0, 3.5, 4.0, 4.5, 5.0)

report_list = []

while True:
    grade = input('Type the grade: ')

    if grade == '':
        break

    grade = float(grade)

    if grade in GRADE_SCALE:
        report_list.append(grade)
    else:
        print('There is no such grade')

if report_list:
    mean = sum(report_list)/len(report_list)
else:
    mean = 0

print(mean)
