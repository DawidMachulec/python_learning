from pprint import pprint

TEXT = """
    We choose to go to the Moon.
    We choose to go to the Moon in this decade and do the other things.
    Not because they are easy, but because they are hard.
    Because that goal will serve to organize and measure the best of our energies and skills.
    Because that challenge is one that we are willing to accept.
    One we are unwilling to postpone.
    And one we intend to win
"""

result = {
    'total_sentences': 0,
    'total_words': 0,
    'total_chars': 0,
    'total_letters': 0,
    'total_commas': 0,
    'total_adverbs': 0
}

for sentence in TEXT.split('.'):
    sentence = sentence.strip()
    result['total_sentences'] += 1
    result['total_chars'] += len(sentence)

    words = sentence.split(' ')
    result['total_words'] += len(words)

    for word in words:
        word = word.strip()
        if ',' in word:
            result['total_letters'] += len(word) - 1
            result['total_commas'] += 1
        else:
            result['total_letters'] += len(word)

        if 'ly' in word:
            result['total_adverbs'] += 1

pprint(result)