from pprint import pprint

MONTHS = ['January', 'February', 'March', 'April',
          'May', 'June', 'July', 'August', 'September',
          'October', 'November', 'December']

result = {f'{i:02d}': element for i, element in enumerate(MONTHS)}
pprint(result)
