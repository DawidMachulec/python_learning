PL = {'ą': 'a', 'ć': 'c', 'ę': 'e',
      'ł': 'l', 'ń': 'n', 'ó': 'o',
      'ś': 's', 'ż': 'z', 'ź': 'z'}

DATA = 'zażółć gęślą jaźń'

result = ''

i = 0
while i < len(DATA):
    if DATA[i] in PL:
        result += PL[DATA[i]]
    else:
        result += DATA[i]
    i += 1

print(result)