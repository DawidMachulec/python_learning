result = [
    (1, 2, 3),
    [1.1, 1.2, 1.3],
    {'aaa', 'bbb', 'ccc'}
]

print(f'Result: {result}\nElements: {len(result)}')
