from datetime import datetime, timedelta

MONTH = 30.436875  # days
YEAR = 365.2425  # days

# duration to shift
duration_years = 8 * YEAR
duration_months = 3 * MONTH
duration_days = 8
duration_hours = 20
duration_minutes = 49
duration_seconds = 15

today = datetime.now()

duration = timedelta(
    days=(duration_days + duration_years + duration_months),
    hours=duration_hours,
    minutes=duration_minutes,
    seconds=duration_seconds)

result_date = today - duration

my_birth = datetime(1994, 2, 28)
my_age = int((result_date - my_birth).days/YEAR)

print(result_date)
print(my_age)
