from datetime import datetime


armstrong = '"July 21st, 1969 2:56:15 AM UTC"'

armstrong = datetime.strptime(armstrong, '"%B %dst, %Y %I:%M:%S %p %Z"')

result = armstrong.strftime('%m/%d/%y %#I:%M %p')
print(result)
