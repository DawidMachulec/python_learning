from datetime import datetime
from pytz import timezone

DATA = '1969-07-21 02:56:15 UTC'

UTC = timezone('UTC')
WAW = timezone('Europe/Warsaw')
BAIKONUR = timezone('Asia/Almaty')

format = '%Y-%m-%d %H:%M:%S UTC'

dt = datetime.strptime(DATA, format)
print(UTC.localize(dt))
print(WAW.localize(dt))
print(BAIKONUR.localize(dt))
