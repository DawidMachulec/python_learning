import json
from pprint import pprint

PATH = 'iris_deserialize.json'

with open(PATH) as file:
    DATA = json.load(file)

header = set()
result = list()

for row in DATA:
    header.update(list(row.keys()))

result.append(header)
for row in DATA:
    result.append(tuple(row.get(head, None) for head in header))

pprint(result)
