import csv

FILE = r'output5.csv'


class Contact:
    def __init__(self, firstname, lastname, addresses=()):
        self.firstname = firstname
        self.lastname = lastname
        self.addresses = addresses


class Address:
    def __init__(self, location, city):
        self.location = location
        self.city = city


DATA = [
    Contact(firstname='Jan', lastname='Twardowski', addresses=(
        Address(location='Johnson Space Center', city='Houston, TX'),
        Address(location='Kennedy Space Center', city='Merritt Island, FL'),
        Address(location='Jet Propulsion Laboratory', city='Pasadena, CA'),
    )),
    Contact(firstname='Mark', lastname='Watney'),
    Contact(firstname='Melissa', lastname='Lewis', addresses=()),
]

astronauts = []

for astronaut in DATA:
    experience = []

    for mission in astronaut.addresses:
        exp = ', '.join([str(x) for x in astronaut.__dict__.values()])
        experience.append(exp)

    astro = astronaut.__dict__
    astro['experience'] = '; '.join(experience)
    astronauts.append(astro)


fieldnames = set()

for contact in astronauts:
    for field_name in contact.keys():
        fieldnames.add(field_name)


with open(FILE, mode='w', encoding='utf-8') as file:
    result = csv.DictWriter(
        f=file,
        fieldnames=sorted(fieldnames, reverse=True),
        delimiter=',',
        quotechar='"',
        quoting=csv.QUOTE_ALL,
        lineterminator='\n')

    result.writeheader()
    result.writerows(astronauts)
