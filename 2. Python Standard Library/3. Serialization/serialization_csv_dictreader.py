import csv

FILE = r'iris.csv'

FIELDNAMES = [
    'Sepal Length',
    'Sepal Width',
    'Petal Length',
    'Petal Width',
    'Species',
]

with open(FILE) as file:
    header = file.readline()

    result = csv.DictReader(
        f=file,
        fieldnames=FIELDNAMES,
        delimiter=',',
        quoting=csv.QUOTE_NONE)

    for row in result:
        print(row)
