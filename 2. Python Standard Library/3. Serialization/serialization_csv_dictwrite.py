import csv

DATA = [
    {'firstname': 'Jan',  'lastname': 'Twardowski'},
    {'firstname': 'José', 'lastname': 'Jiménez'},
    {'firstname': 'Mark', 'lastname': 'Watney'},
    {'firstname': 'Ivan', 'lastname': 'Ivanovic'},
    {'firstname': 'Melissa', 'lastname': 'Lewis'},
]

PATH = 'output.csv'

fieldnames = DATA[0].keys()

with open(PATH, mode='w', encoding='utf-8') as file:
    result = csv.DictWriter(
        f=file,
        fieldnames=fieldnames,
        delimiter=',',
        quotechar='"',
        quoting=csv.QUOTE_ALL,
        lineterminator='\n')

    result.writeheader()
    result.writerows(DATA)
