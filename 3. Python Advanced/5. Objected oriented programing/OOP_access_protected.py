"""
>>> DATA = [Iris(5.8, 2.7, 5.1, 1.9, 'virginica'),
...         Iris(5.1, 3.5, 1.4, 0.2, 'setosa'),
...         Iris(5.7, 2.8, 4.1, 1.3, 'versicolor')]
>>> result = [{attribute: value}
...           for row in DATA
...           for attribute, value in row.__dict__.items()
...           if not attribute.startswith('_')]
>>> result  # doctest: +NORMALIZE_WHITESPACE
[{'species': 'virginica'},
 {'species': 'setosa'},
 {'species': 'versicolor'}]

>>> DATA2 = [Iris2(5.8, 2.7, 5.1, 1.9, 'virginica'),
...         Iris2(5.1, 3.5, 1.4, 0.2, 'setosa'),
...         Iris2(5.7, 2.8, 4.1, 1.3, 'versicolor')]
>>> result2 = [{attribute: value}
...           for row in DATA2
...           for attribute, value in row.__dict__.items()
...           if not attribute.startswith('_')]
>>> result2  # doctest: +NORMALIZE_WHITESPACE
[{'species': 'virginica'},
 {'species': 'setosa'},
 {'species': 'versicolor'}]
"""
from dataclasses import dataclass


class Iris:
    def __init__(self, sepal_length, sepal_width, petal_length, petal_width, species):
        self.species = species
        self.__petal_width = petal_width
        self.__petal_length = petal_length
        self.__sepal_width = sepal_width
        self.__sepal_length = sepal_length


@dataclass
class Iris2:
    __sepal_length: float
    __sepal_width: float
    __petal_length: float
    __petal_width: float
    species: str