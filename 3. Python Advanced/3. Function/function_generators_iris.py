"""
>>> from sys import getsizeof
>>> from inspect import isfunction, isgeneratorfunction
>>> assert isfunction(function)
>>> assert isgeneratorfunction(generator)
>>> list(function(DATA, 'setosa'))
[[5.1, 3.5, 1.4, 0.2], [4.7, 3.2, 1.3, 0.2]]
>>> list(generator(DATA, 'setosa'))
[[5.1, 3.5, 1.4, 0.2], [4.7, 3.2, 1.3, 0.2]]
>>> getsizeof(function(DATA, 'setosa'))
88
>>> getsizeof(function(DATA*10, 'setosa'))
248
>>> getsizeof(function(DATA*100, 'setosa'))
1656
>>> getsizeof(generator(DATA, 'setosa'))
112
>>> getsizeof(generator(DATA*10, 'setosa'))
112
>>> getsizeof(generator(DATA*100, 'setosa'))
112
"""

DATA = [(5.8, 2.7, 5.1, 1.9, 'virginica'),
        (5.1, 3.5, 1.4, 0.2, 'setosa'),
        (5.7, 2.8, 4.1, 1.3, 'versicolor'),
        (6.3, 2.9, 5.6, 1.8, 'virginica'),
        (6.4, 3.2, 4.5, 1.5, 'versicolor'),
        (4.7, 3.2, 1.3, 0.2, 'setosa')]


def function(data: list, species: str):
    result = []
    for *features, label in data:
        if label == species:
            result.append(features)
    return result


def generator(data: list, species: str):
    for *features, label in data:
        if label == species:
            yield features
