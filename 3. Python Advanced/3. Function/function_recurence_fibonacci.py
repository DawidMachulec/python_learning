"""
>>> fib(1)
1
>>> fib(2)
2
>>> fib(5)
8
>>> fib(9)
55
>>> fib(10)
89
>>> [fib(x) for x in range(10)]
[1, 1, 2, 3, 5, 8, 13, 21, 34, 55]
"""


def fib(n):
    if n <= 1:
        return 1
    else:
        return fib(n-1) + fib(n-2)
