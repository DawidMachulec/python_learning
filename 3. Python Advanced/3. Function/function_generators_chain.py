"""
>>> result
11502.0
"""


def odd(x):
    return x % 2


def cube(x):
    return x ** 3


result = (x for x in range(1, 34) if x % 3 == 0)
result = filter(odd, result)
result = map(cube, result)
list_result = list(result)
result = sum(list_result) / len(list_result)
