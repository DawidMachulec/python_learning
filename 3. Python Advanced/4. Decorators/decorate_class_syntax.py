"""
>>> from inspect import isclass
>>> assert isclass(MyDecorator)
>>> assert isinstance(MyDecorator(lambda: None), MyDecorator)
>>> @MyDecorator
... def echo(text):
...     return text
>>> echo('hello')
'hello'
"""


class MyDecorator:
    def __init__(self, func):
        self._func = func

    def __call__(self, *args, **kwargs):
        return self._func(*args, **kwargs)
    