"""
>>> from inspect import isfunction
>>> assert isfunction(mydecorator)
>>> assert isfunction(mydecorator(a=1, b=2))
>>> assert isfunction(mydecorator(a=1, b=2)(lambda: None))
>>> @mydecorator(a=1, b=2)
... def echo(text):
...     return text
>>> echo('hello')
'hello'
"""


def mydecorator(a=1, b=2):
    def decorator(func):
        def wrapper(*args, **kwargs):
            return func(*args, **kwargs)
        return wrapper
    return decorator
