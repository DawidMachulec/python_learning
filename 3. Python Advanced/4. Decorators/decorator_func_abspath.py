"""
>>> @abspath
... def display(path):
...     return str(path)
>>> display('iris.csv').startswith(str(CURRENT_DIR))
True
>>> display('iris.csv').endswith('iris.csv')
True
>>> display('/home/python/iris.csv')
'/home/python/iris.csv'
"""

from pathlib import Path

CURRENT_DIR = Path().cwd()


def abspath(func):
    def wrapper(path: str):
        if not path.startswith(str(CURRENT_DIR)):
            path = Path(CURRENT_DIR, path)
        return func(path)
    return wrapper
