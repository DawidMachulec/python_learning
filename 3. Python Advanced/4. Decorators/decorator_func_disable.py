"""
>>> from inspect import isfunction
>>> assert isfunction(disable)
>>> assert isfunction(disable(lambda: None))
>>> @disable
... def echo(text):
...     print(text)
>>> echo('hello')
Traceback (most recent call last):
PermissionError: Function is disabled
"""


def disable(func):
    def wrapper(*args, **kwargs):
        raise PermissionError('Function is disabled')
    return wrapper
