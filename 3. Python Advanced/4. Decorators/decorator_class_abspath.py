"""
>>> @Abspath
... def display(path):
...     return str(path)
>>> display('iris.csv').startswith(str(CURRENT_DIR))
True
>>> display('iris.csv').endswith('iris.csv')
True
>>> display('/home/python/iris.csv')
'/home/python/iris.csv'
"""

from pathlib import Path


CURRENT_DIR = Path().cwd()


class Abspath:
    def __init__(self, func):
        self._func = func

    def __call__(self, file):
        file = Path(CURRENT_DIR, file)
        return self._func(file)
