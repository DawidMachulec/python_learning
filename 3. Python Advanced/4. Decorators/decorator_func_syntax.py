"""
>>> from inspect import isfunction
>>> assert isfunction(mydecorator)
>>> assert isfunction(mydecorator(lambda: None))
>>> @mydecorator
... def echo(text):
...     return text
>>> echo('hello')
'hello'
"""


def mydecorator(func):
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)
    return wrapper
