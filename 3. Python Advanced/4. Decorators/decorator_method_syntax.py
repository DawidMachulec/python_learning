"""
>>> from inspect import isfunction
>>> assert isfunction(mydecorator)
>>> assert isfunction(mydecorator(lambda: None))
>>> class MyClass:
...     @mydecorator
...     def echo(text):
...         return text
>>> my = MyClass()
>>> my.echo('hello')
'hello'
"""


def mydecorator(method):
    def wrapper(self, *args, **kwargs):
        return method(*args, **kwargs)
    return wrapper
