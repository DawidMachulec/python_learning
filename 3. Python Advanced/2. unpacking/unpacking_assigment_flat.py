"""
>>> type(ip)
<class 'str'>
>>> type(hosts)
<class 'list'>
>>> assert all(type(host) is str for host in hosts)
>>> '' not in hosts
True
>>> ip
'10.13.37.1'
>>> hosts
['nasa.gov', 'esa.int', 'roscosmos.ru']
"""

DATA = '10.13.37.1      nasa.gov esa.int roscosmos.ru'

ip: str
hosts: list

ip, *hosts = DATA.strip().split()
