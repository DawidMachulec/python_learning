"""
>>> isnumeric()
False
>>> isnumeric(0)
True
>>> isnumeric(1)
True
>>> isnumeric(-1)
True
>>> isnumeric(1.1)
True
>>> isnumeric('one')
False
>>> isnumeric([1, 1.1])
False
>>> isnumeric(1, 1.1)
True
>>> isnumeric(1, 'one')
False
>>> isnumeric(1, 'one', 'two')
False
>>> isnumeric(True)
False
"""


def isnumeric(*args):
    searching_type = (int, float)

    if not args:
        return False

    for element in args:
        if type(element) not in searching_type:
            return False

    return True
