"""
>>> mean(1)
1.0
>>> mean(1, 3)
2.0
>>> mean()
Traceback (most recent call last):
ValueError: At least one argument is required
"""


def mean(*args):
    if len(args):
        return sum(args)/len(args)
    else:
        raise ValueError('At least one argument is required')
