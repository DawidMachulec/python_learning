"""
>>> isnumeric()
False
>>> isnumeric(0)
True
>>> isnumeric(1)
True
>>> isnumeric(-1)
True
>>> isnumeric(1.1)
True
>>> isnumeric('one')
False
>>> isnumeric([1, 1.1])
False
>>> isnumeric(1, 1.1)
True
>>> isnumeric(1, 'one')
False
>>> isnumeric(1, 'one', 'two')
False
>>> isnumeric(True)
False
>>> isnumeric(a=1)
True
>>> isnumeric(a=1.1)
True
>>> isnumeric(a='one')
False
"""


def isnumeric(*args, **kwargs):
    searching_type = (int, float)
    arguments = args + tuple(kwargs.values())

    if not arguments:
        return False

    for element in arguments:
        if type(element) not in searching_type:
            return False

    return True
