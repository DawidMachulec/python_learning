"""
>>> type(result)
<class 'set'>
>>> 'virginica' in result
True
>>> 'setosa' in result
True
>>> 'versicolor' in result
False
"""

DATA = [('Sepal length', 'Sepal width', 'Petal length', 'Petal width', 'Species'),
        (5.8, 2.7, 5.1, 1.9, {'virginica'}),
        (5.1, 3.5, 1.4, 0.2, {'setosa'}),
        (5.7, 2.8, 4.1, 1.3, {'versicolor'}),
        (6.3, 2.9, 5.6, 1.8, {'virginica'}),
        (6.4, 3.2, 4.5, 1.5, {'versicolor'}),
        (4.7, 3.2, 1.3, 0.2, {'setosa'}),
        (7.0, 3.2, 4.7, 1.4, {'versicolor'}),
        (7.6, 3.0, 6.6, 2.1, {'virginica'}),
        (4.6, 3.1, 1.5, 0.2, {'setosa'})]

SUFFIXES = ('ca', 'osa')

result: set = set()

header, *measurement = DATA

for *feature, species in measurement:
    for element in species:
        if str(element).endswith(SUFFIXES):
            result.add(element)
