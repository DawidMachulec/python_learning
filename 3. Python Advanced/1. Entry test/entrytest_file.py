"""
>>> assert type(result) is list
>>> assert all(type(row) is dict for row in result)
>>> result  # doctest: +NORMALIZE_WHITESPACE
[{'ip': '127.0.0.1', 'hosts': ['localhost', 'astromatt'], 'protocol': 'ipv4'},
 {'ip': '10.13.37.1', 'hosts': ['nasa.gov', 'esa.int', 'roscosmos.ru'], 'protocol': 'ipv4'},
 {'ip': '255.255.255.255', 'hosts': ['broadcasthost'], 'protocol': 'ipv4'},
 {'ip': '::1', 'hosts': ['localhost'], 'protocol': 'ipv6'}]
"""

DATA = """
##
# `/etc/hosts` structure:
#   - IPv4 or IPv6
#   - Hostnames
 ##

127.0.0.1       localhost
127.0.0.1       astromatt
10.13.37.1      nasa.gov esa.int roscosmos.ru
255.255.255.255 broadcasthost
::1             localhost
"""

result = []


def check_result(x):
    position = -1

    for i, element in enumerate(result):
        if x in element.values():
            position = i

    return position


for row in DATA.strip().split('\n'):
    if row.startswith(('#', ' ', '\n', '\t')) or not row:
        continue

    ip, *host = row.split()

    protocol = ('IPv4' if ip.find('.') != -1 else 'IPv6')

    check_output = check_result(ip)
    if check_output == -1:
        result.append({
            'ip': ip,
            'host': host,
            'protocol': protocol
        })
    else:
        result[check_output]['host'] += host
